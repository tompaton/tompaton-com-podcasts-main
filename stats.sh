#!/bin/bash
if [ ! -f stats/downloaded.csv ]; then
  echo "DateTime,Count,RawSize,ProcessedSize" > stats/downloaded.csv
fi

echo "$(date -u +%s),$(ls -1 downloaded/ | wc -l),$(du downloaded/ | cut -f 1),$(du processed/ | cut -f 1)" >> stats/downloaded.csv
