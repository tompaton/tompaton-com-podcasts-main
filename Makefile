MP3S = $(wildcard downloaded/*.mp3)
PROCESSED = $(patsubst downloaded/%.mp3,processed/%.mp3,$(MP3S))

.PHONY: process
process: $(PROCESSED)
	@echo Done.

processed/%.mp3: downloaded/%.mp3
	@echo "Processing $^"
	@nice sox --no-show-progress --multi-threaded \
		--buffer 32768 --norm "$<" "$@" \
		tempo -s 1.33 channels 1 rate 44100
	@echo "Done $^"
