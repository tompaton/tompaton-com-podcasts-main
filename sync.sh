#!/bin/bash
log="logs/sync-$(date --iso-8601=seconds).log"
echo "Logging to $log..."
exec > /podcasts/$log
exec 2>&1

cd /podcasts

echo "Purging deleted..."
find deleted/ -name '*.mp3' -type f -mtime +1 -delete

echo "Downloading..."
greg --configfile /src/greg.conf sync

echo "Downloading RSS..."
/src/proxy_rss.sh

echo "Fixing names..."
shopt -s nullglob
for m in downloaded/*.mp3_* ; do mv $m ${m/.mp3/}.mp3; done
for m in downloaded/https* ; do mv $m ${m/*%2F/}; done

echo "Removing failed downloads..."
find downloaded/ -size 0 -delete

echo "Processing..."
cp /src/Makefile .
make -j2

echo "Generating rss..."
/src/rss.sh

echo "Collecting stats..."
/src/stats.sh

echo "All done."
echo
