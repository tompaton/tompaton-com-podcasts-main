#!/usr/bin/env python

import sys

from pathlib import Path

from rss import get_entry, processed, downloaded

if __name__ == '__main__':
    if len(sys.argv) == 2:
        mp3 = sys.argv[1]
        entry = get_entry(downloaded / mp3, processed / mp3)

        print('{filename}\n'
              '{url}\n'
              '{author}\n'
              '{title}\n'
              '{duration_seconds}\n'
              '{length}\n'
              .format(**entry))

    else:
        print('Usage: {} <file_name>'.format(__file__))
