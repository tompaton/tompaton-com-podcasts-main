#!/usr/bin/env python
import sys


HTML = """
<html>
    <head>
        <title>podcasts.tompaton.com</title>
        <link rel="alternate" type="application/rss+xml" href="https://podcasts.tompaton.com/downloaded.rss" />
    </head>
    <body>
        <h1>podcasts.tompaton.com</h1>
        <a rel="alternate" type="application/rss+xml" href="downloaded.rss">downloaded rss</a>
        <a href="downloaded.html">downloaded html</a>

        <h2>Feeds</h2>
        <ul>
            {}
        </ul>
    </body>
</html>
""".strip()


def parse_info(lines):
    feed = url = None
    for line in lines:
        if line:
            if line[0].isalnum():
                feed = line.strip()
            if line.startswith('    url: '):
                url = line.strip()[5:]
                if 'samharris' in url or 'JohnRoderic' in url or 'listennotes' in url:
                    continue
                yield feed, url


def proxy_rss(url):
    name = url.split('//')[1].replace('?', '_')
    if not name.endswith('.xml'):
        name += '.xml'
    if name.endswith('/.xml'):
        name = name[:-4] + 'index.html.xml'
    return 'https://podcasts.tompaton.com/proxy_rss/' + name


if __name__ == "__main__":
    print(HTML.format("\n".join("""<li><a href="downloaded.html?{url2}">{feed}</a> (<a href="{url}">src</a>)</li>"""
                                .format(url=url, feed=feed,
                                        url2=proxy_rss(url))
                                for feed, url in sorted(parse_info(sys.stdin.readlines())))))
