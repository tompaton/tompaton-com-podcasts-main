#!/bin/bash
for url in $(greg --configfile /src/greg.conf info | grep -Po '(?<=url: )(.+)$' | grep -v samharris | grep -v JohnRoderick | grep -v listennotes ); do
    wget --no-verbose --force-directories --directory-prefix=proxy_rss $url ;
done

python -c 'from pathlib import Path; [f.rename(f.with_name(f.name.replace("?", "_"))) for f in Path("proxy_rss/").glob("**/*") if f.is_file and "?" in f.name]'

find proxy_rss/ -type f -not -name "*.xml" -exec mv --force -T {} {}.xml \;

greg --configfile /src/greg.conf info | python3 /src/proxy_rss.py > index.html
