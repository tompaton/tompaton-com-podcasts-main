#!/bin/bash
usage () {
    echo "$0 [list|play|rm|undo|save|download] <mp3>"
}

list_mp3s () {
    echo "Downloaded files:"
    if [[ $1 ]] ; then
        ls -1tr downloaded/ | nl | grep -i $1
    else
        ls -1tr downloaded/ | nl
    fi
}

rm_mp3 () {
    mp3=$(_mp3 $1)
    echo "Remove download $mp3..."
    if [ -f "processed/$mp3" ]; then
        rm processed/$mp3
        mv downloaded/$mp3 deleted/
        touch deleted/$mp3
        _rss
    elif [ -f "archived/$mp3" ]; then
        echo "Removed from archive!"
        mv archived/$mp3 deleted/
        touch deleted/$mp3
    else
        echo "Not found!"
    fi
}

undo_rm_mp3s () {
    echo "Restore deleted downloads..."
    mv deleted/$mp3 downloaded/
    _rss
}

save_mp3 () {
    mp3=$(_mp3 $1)
    echo "Archive download $mp3..."
    if [ -f "processed/$mp3" ]; then
        rm processed/$mp3
        mv downloaded/$mp3 archived/
        _rss
    elif [ -f "deleted/$mp3" ]; then
        echo "Restored from deleted!"
        mv deleted/$mp3 archived/
    else
        echo "Not found!"
    fi
}

play_mp3 () {
    mp3=$(_mp3 $1)
    if [ -f "processed/$mp3" ]; then
        # echo "https://podcasts.tompaton.com/downloaded/$mp3"
        # echo "/podcasts/processed/$mp3"
        python3 episode_info.py "$mp3"
    else
        echo "Not found!"
    fi
}

download_mp3 () {
    wget --quiet -P downloaded/ $1
    make
    _rss
}

_mp3 () {
    re='^[0-9]+$'
    if [[ $1 =~ $re ]] ; then
        ls -1tr downloaded/ | sed "${1}q;d"
    else
        echo $1
    fi
}

_rss () {
    ./rss.sh
    ./stats.sh
}

case "$1" in
    list ) list_mp3s "$2" ;;
    play ) [[ $2 ]] && play_mp3 "$2" || usage ;;
    rm | remove | delete ) [[ $2 ]] && rm_mp3 "$2" || usage ;;
    save | archive ) [[ $2 ]] && save_mp3 "$2" || usage ;;
    download ) [[ $2 ]] && download_mp3 "$2" || usage ;;
    undo ) undo_rm_mp3s ;;
    * ) usage ;;
esac
