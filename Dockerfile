FROM python:3.12-bullseye

RUN apt-get update && apt-get install -y libsox-fmt-mp3 sox cron

RUN pip install --upgrade pip

RUN mkdir -p /podcasts/data /podcasts/stats /podcasts/proxy_rss \
             /podcasts/downloaded /podcasts/processed /podcasts/archived \
             /podcasts/deleted /podcasts/progress /podcasts/images \
             /podcasts/logs \
             /src

COPY * /src/
RUN pip install --no-cache-dir -r /src/requirements.txt

ENV BETTER_EXCEPTIONS=1

WORKDIR /podcasts

RUN ln -s /src/sync.sh /etc/cron.hourly/sync

ENTRYPOINT ["cron", "-f"]

ENV TZ="Australia/Melbourne"
